const express = require("express");
const router = express.Router();
const userModel = require("../models/userModel");
const users = [
  {
    username: "admin@test.com",
    password: "123456",
  },
];

router.get("/users", (req, res) => {
  return res.send({ users });
});

router.post("/user", (req, res) => {
  // #1 validate req.body with requirement, if there is some error then return it.
  // #2 "find" the user from the arrays with "not match username and password"
  // #3 return the user back if user does not exist
  // #4 return error if the user exist with status code "400" => Bad request
  const { error } = userModel.validate(req.body);
  const user = users.find((user) => {
    return user.username == req.body.username;
  });
  if (error) {
    return res.status(400).send({ error: error.details[0].message });
  }
  if (user) {
    return res.status(400).send({ error: "Duplicate username" });
  }
  users.push(req.body)
  return res.send(req.body);
});


router.post("/login", (req, res) => {
  // #1 validate req.body with requirement, if there is some error then return it.
  // #2 "find" the user from the arrays with "match username and password"
  // #3 return the user back it it authenticated
  // #4 return error if the password or username did not correct with status code "401" Unathorized
  const { error } = userModel.validate(req.body);
  const user = users.find((user) => {
    return (
      user.username == req.body.username && user.password === req.body.password
    );
  });
  if (error) {
    return res.status(400).send({ error: error.details[0].message });
  }
  if (user) {
    return res.send(`Hello ${req.body.username}`);
  }
  return res.send({ error: "invalid username or password" });
});

module.exports = router;
